These theme files are edited versions of the example file from 
/usr/doc/elilo-3.16/examples/textmenu_chooser/textmenu-message.msg 
on the Slackware Linux system.

Elilo uses the textmenu-message.msg file to draw the attributes of its
dialog box boot screen from which the kernel can be selected with the
cursor Up/Down keys.

These dialog themes consist of various colours including a solid blue and solid 
red box and other transparent themes coloured cyan,green,grey,red and yellow.

Solid Blue theme.


![Screenshot of elilo blue textmenu](elilo-blue.png "Screenshot of elilo blue textmenu")

More screenshots at the bottom of the page.

**Use:**

textmenu-message.msg is basically a text file containing text and graphic characters along
with control codes to specify colour attributes, menu and prompt positions. Graphic characters
are taken from the standard IBM VGA set.

These textmenu-message.msg files work on Slackware current 5.11.8 x86_64, screen resolution 
1920x1080 with elilo.efi and elilo.conf invoked from the EFI System Partition. 

I expect these msg files to be compatible with other Linux operating systems that use elilo 
or have a different screen resolution.


**Installation:**

Download the tar.gz in the web browser by clicking the download button on the top right of this
page or use git to clone the files.

```
git clone https://gitlab.com/ethelack/elilo-textmenu-message-themes ~/elilo-textmenu-message-themes

```

Make sure that the ESP partition is mounted at /boot/efi before you copy the files over.

As Root copy the msg files to the same location as elilo.conf. 

```
mount | grep /boot/efi

find /boot -name "elilo.conf"                   # find the location of elilo.conf

cd ~/elilo-textmenu-message-themes

su                                              # switch user to root or run the next 
                                                # cp command with sudo

cp *.msg /boot/efi/EFI/Slackware                # copy msg files to the same location 
                                                # as elilo.conf 
                                                # In this case /boot/efi/EFI/Slackware

ls -l /boot/efi/EFI/Slackware/                  # check msg files were copied over

```


**Edit /boot/efi/EFI/Slackware/elilo.conf**
```
You will have to hash out or remove the following two simple menu options.
#chooser=simple
#message=boot_message.txt 

Then add in the textmenu options:
"chooser=textmenu" to instruct elilo to run the textmenu on reboot.
"message=" option to specify the name of the textmenu-message.msg file to use as the theme.

It is recommended to add the "f1" and "f2" options so that help information is accesible 
from the boot menu. The f1 "General" and f2 "Params" help files are themed in similar 
colours to the textmenu-message files.

The colours are:
blue            green-trans         yellow-trans
red             grey-trans
cyan-trans      red-trans

The "prompt" option instructs elilo to enter interactive mode so that the user can select 
a kernel label at the boot prompt. "prompt" has to be used to enable the textmenu dialog 
box to be displayed.

"delay" option is used for the simple menu. It is the amount of time in 10ths/sec when not 
in interactive mode to wait before auto booting. Default is 0.

"timeout" option is the amount of time to wait in 10ths/sec when in textmenu interactive 
mode before auto booting the default kernel. Default is infinity.

Note - On my system the setting of timeout is not having any effect, the autoboot is 
defaulting to infinity. The fact that the timeout can not be adjusted for the textmenu 
appears to be a bug in elilo upstream. 

If you want elilo to autoboot you will have to use the simple menu and delay option.

More info here 
https://www.linuxquestions.org/questions/slackware-14/elilo-timeout-not-working-4175584936/


An example elilo.conf

## Global section ##
#chooser=simple
#delay=50
#message=boot_message.txt

prompt

chooser=textmenu
timeout=50
message=textmenu-message-green-trans.msg
f1=General-green-trans.msg
f2=Params-green-trans.msg

default=linux-huge-5.13.12

## Stanzas ## 

image=vmlinuz-huge-5.13.12
    label=linux-huge-5.13.12
    description="huge-5.13.12"
    read-only
    append="root=/dev/sda6 vga=normal ro"

image=vmlinuz-generic-5.14.0
    label=linux-generic-5.14.0
    description="generic-5.14.0"
    initrd=initrd.gz
    read-only
    append="root=/dev/sda6 vga=normal ro"

# dummy stanza to boot windows.
image=no_windows
	label=Windows-10
	read-only

```
**Screenshots**

Solid Red

![Screenshot of elilo red textmenu](elilo-red.png "Screenshot of elilo red textmenu")

Cyan Transparent

![Screenshot of elilo cyan trans textmenu](elilo-cyan-trans.png "Screenshot of elilo cyan trans textmenu")

Green Transparent

![Screenshot of elilo green trans textmenu](elilo-green-trans.png "Screenshot of elilo green trans textmenu")

Grey Transparent

![Screenshot of elilo grey trans textmenu](elilo-grey-trans.png "Screenshot of elilo grey trans textmenu")

Red Transparent

![Screenshot of elilo red-trans textmenu](elilo-red-trans.png "Screenshot of elilo red trans textmenu")

Yellow Transparent

![Screenshot of elilo yellow trans textmenu](elilo-yellow-trans.png "Screenshot of elilo yellow-trans textmenu")

F1 blue Help menu

![Screenshot of elilo F1 blue page](elilo-F1-blue.png "Screenshot of elilo F1 blue page")

F2 blue Help menu

![Screenshot of elilo F2 blue page](elilo-F2-blue.png "Screenshot of elilo F2 blue page")
 
F1 green Help menu

![Screenshot of elilo F1 green page](elilo-F1-green-trans.png "Screenshot of elilo F1 green page")

F2 green Help menu

![Screenshot of elilo F2 green page](elilo-F2-green-trans.png "Screenshot of elilo F2 green page")






